(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(("ELPA"  . "http://tromey.com/elpa/")
			 ("gnu"   . "http://elpa.gnu.org/packages/")
			 ("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")))
(package-initialize)


(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(setq use-package-always-ensure t)
(org-babel-load-file (expand-file-name "~/.emacs.d/config.org"))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#2e3436" :foreground "#eeeeec" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 105 :width normal :foundry "PfEd" :family "Source Code Pro")))))
(put 'downcase-region 'disabled nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   (quote
    ("~/Sync/Projects/EE/EE.org")))
 '(org-journal-date-formats "%A, %d %B %Y" t)
 '(org-journal-dir "~/org/Journal/")
 '(org-journal-enable-encryption t)
 '(org-journal-encrypt-journal t)
 '(package-selected-packages
   (quote
    (org-agenda-property zerodark-theme yasnippet-snippets which-key vterm sudo-edit spaceline solarized-theme slime-company quelpa-use-package org-ref org-journal org-bullets memrise meghanada matrix-client mastodon ido-vertical-mode exwm evil-org evil-magit evil-collection elpy elfeed dmenu dashboard))))
